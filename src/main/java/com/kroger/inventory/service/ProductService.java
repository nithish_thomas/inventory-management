package com.kroger.inventory.service;

import com.kroger.inventory.persistance.model.Product;
import com.kroger.inventory.persistance.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    private ProductRepository repository;

    @Autowired
    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    public List<Product> getAllProducts(){
        List<Product> products = repository.findAll();
        return products;
    }

    public Product addNewProduct(Product newProduct) {
        return repository.save(newProduct);
    }
}

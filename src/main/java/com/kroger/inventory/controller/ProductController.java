package com.kroger.inventory.controller;


import com.kroger.inventory.dto.ProductDTO;
import com.kroger.inventory.persistance.model.Product;
import com.kroger.inventory.service.ProductService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(
  path = "/api/products"
)
public class ProductController {
    private static final Logger logger=LoggerFactory.getLogger(ProductController.class);

    private ModelMapper modelMapper;
    private ProductService productService;

    @Autowired
    public ProductController(ModelMapper modelMapper, ProductService productService) {
        this.modelMapper = modelMapper;
        this.productService = productService;
    }

    @GetMapping
    public List<ProductDTO> getAllProducts() {
        logger.debug("getAllProducts called");
        Type listType = new TypeToken<List<ProductDTO>>() {
        }.getType();
        List<ProductDTO> productRespons = modelMapper.map(productService.getAllProducts(), listType);
        return productRespons;
    }

    @PostMapping
    public ProductDTO addProduct(@RequestBody ProductDTO productAddRequest) {
        Product addedProduct = productService.addNewProduct(modelMapper.map(productAddRequest, Product.class));
        return modelMapper.map(addedProduct, ProductDTO.class);
    }
}

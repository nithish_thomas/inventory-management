package com.kroger.inventory.service;

import com.kroger.inventory.persistance.model.Product;
import com.kroger.inventory.persistance.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    private ProductService service;

    @Mock
    private ProductRepository repository;

    @Before
    public void setUp() {
        service = new ProductService(repository);
    }

    @Test
    public void getAllProducts() {
        final Date manufactureDate = new Date();
        final List<Product> test_product = Arrays.asList(new Product(1L, "test product", manufactureDate,"test category"));
        when(repository.findAll()).thenReturn(test_product);
        List<Product> products = service.getAllProducts();

        assertSame(test_product, products);
    }

    @Test
    public void addNewProduct() {
        final String productName = "product";
        final String category = "test category";
        Product product = new Product(null, productName, new Date(), category);
        when(repository.save(product)).thenAnswer((invocation) -> {
            Product productArg = invocation.getArgument(0);
            productArg.setId(1L);
            return productArg;
        });

        final Product savedProduct = service.addNewProduct(product);
        assertNotNull(savedProduct);
        assertEquals((Long) 1L, savedProduct.getId());
        assertEquals(productName, savedProduct.getName());
        assertEquals(category, savedProduct.getCategory());
    }
}
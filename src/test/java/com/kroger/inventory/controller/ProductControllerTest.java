package com.kroger.inventory.controller;

import com.kroger.inventory.helper.DateHelper;
import com.kroger.inventory.persistance.model.Product;
import com.kroger.inventory.service.ProductService;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
public class ProductControllerTest {

    final Product test_product = new Product(1L, "test product", new Date(),"test category");

    private ProductService productService;
    private ModelMapper modelMapper;

    @Before
    public void setUp() {
        productService=mock(ProductService.class);
        modelMapper=spy(ModelMapper.class);
        RestAssuredMockMvc.standaloneSetup(new ProductController(modelMapper,productService));
        when(productService.getAllProducts()).thenReturn(Arrays.asList(test_product));
    }

    @Test
    public void get_api_products_ShouldReturnAllTheItemsInDB() throws Exception {
        RestAssuredMockMvc.when().get("/api/products").peek()
          .then()
            .statusCode(HttpStatus.SC_OK)
            .body(matchesJsonSchemaInClasspath("jsonSchemas/productcontroller_getAllProducts.json"))
            .body("$",hasSize(1))
            .body("[0].id", equalTo(1))
            .body("[0].name", equalTo(test_product.getName()))
            .body("[0].manufactureDate", equalTo(DateHelper.getSimpleDateFormat().format(test_product.getManufactureDate())));
    }

    @Test
    public void post_api_products_ShouldCreateANewItemInDB() throws Exception {
        final Product productToBeAdded = new Product(null, "product name", DateHelper.getDate("12-Mar-2018"),"test category");
        when(productService.addNewProduct(productToBeAdded)).thenAnswer((invocation) -> {
            Product productArg = invocation.getArgument(0);
            productArg.setId(1L);
            return productArg;
        });

        final String content = "{\"name\":\"product name\",\"manufactureDate\":\"12-Mar-2018\",\"category\":\"test category\"}";
        RestAssuredMockMvc
          .given()
            .body(content)
            .contentType(ContentType.JSON)
            .post("/api/products")
          .then()
            .body("id",equalTo(1))
            .body("name",equalTo("product name"))
            .body("category",equalTo("test category"));
    }

}
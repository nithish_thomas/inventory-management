package com.kroger.inventory.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateHelper {
    public static Date getDate(String date) throws ParseException {
        final SimpleDateFormat sdf = getSimpleDateFormat();
        return sdf.parse(date);
    }

    public static SimpleDateFormat getSimpleDateFormat() {
        final SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf;
    }
}

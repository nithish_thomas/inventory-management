package com.kroger.inventory;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.kroger.inventory.dto.ProductDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestExecutionListeners(listeners = {DbUnitTestExecutionListener.class}, mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS)
public class DemoApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void contextLoads() {
    }

    @Test
    @DatabaseSetup("/testDb/some-sample-products.xml")
    public void api_products_apiShouldReturnAllProductsInDb() {

        ResponseEntity<List<ProductDTO>> responseEntity = restTemplate.exchange(
          "/api/products",
          HttpMethod.GET,
          null,
          new ParameterizedTypeReference<List<ProductDTO>>() {
          });
        List<ProductDTO> products = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(3, products.size());
    }

    @Test
    @DatabaseSetup("/testDb/some-sample-products.xml")
    public void api_products_apiShouldReturnAllProductsInDb2() {

        final String content = "{\"name\":\"product name\",\"manufactureDate\":\"12-Mar-2018\",\"category\":\"test category\"}";

        ResponseEntity<ProductDTO> responseEntity = restTemplate.exchange(
          "/api/products",
          HttpMethod.POST,
          generateJsonRequest(content),
          ProductDTO.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());


        ResponseEntity<List<ProductDTO>> allProducts = restTemplate.exchange(
          "/api/products",
          HttpMethod.GET,
          null,
          new ParameterizedTypeReference<List<ProductDTO>>() {
          });
        List<ProductDTO> products = allProducts.getBody();
        assertEquals(4, products.size());
    }

    private HttpEntity<String> generateJsonRequest(String requestJson) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<String>(requestJson, headers);
    }

}
